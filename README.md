# i18n-excel-tool

A tool to convert i18n properties files into excel documents and viceversa.

## Usage
It's built on [leiningen](https://leiningen.org/).

Usage: i18n-excel-tool [options] action

Options:

  -o, --output OUTPUT  Output Folder if creating Props or File if generating excel

  -i, --input   INPUT  Input Folder if creating excel or Folder if creating props

  -h, --help


Actions:

gen-ex  Generate excel file from i18n properties files

gen-pr  Generate 18n properties files from excel file

Ex:
java -jar target/i18n-excel-tool-0.1.0-standalone.jar -i the_excel.xlsx -o PR_DIR gen-pr

## License

GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
