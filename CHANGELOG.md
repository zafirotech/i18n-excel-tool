# Change Log
All notable changes to this project will be documented in this file.

## [0.1.0] - 2017-02-10
### Added
- First release. Basic functionality in place to generate an excel document based on i18n properties files
as well as generation of the properties  for the excel doc.

