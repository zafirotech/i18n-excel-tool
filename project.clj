(defproject i18n-excel-tool "0.2.0-SNAPSHOT"
  :description "Tool to convert from from i18n properties file into excel and viceversa."
  :url "http://zafirotech.ca/apps/i18n-excel-tool/"
  :license {:name "GNU GENERAL PUBLIC LICENSE"
            :url  "http://www.gnu.org/licenses/"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.apache.poi/poi "3.15"]
                 [org.apache.poi/poi-ooxml "3.15"]
                 [org.clojure/tools.cli "0.3.5"]]
  :main zaf.i18n-excel-tool.core
  :aot [zaf.i18n-excel-tool.core])
