{:default-excel-input-path   "excel-with-properties.xlsx"
 :default-excel-output-path   "excel-with-properties.xlsx"
 :default-input-directory "."
 :default-output-directory "."
 :charset-encoding         "iso-8859-1"
 ;; number of characters wide
 :default-cell-width       40
 :header-text-for-non-lang "Default"}