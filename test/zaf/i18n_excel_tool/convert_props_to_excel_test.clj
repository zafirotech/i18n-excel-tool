(ns zaf.i18n-excel-tool.convert-props-to-excel-test
  (:require [clojure.test :refer :all]
            [zaf.i18n-excel-tool.convert-props-to-excel :refer :all]
            [zaf.i18n-excel-tool.convert-excel-to-props :as etp]
            [zaf.i18n-excel-tool.test-helper :as th])
  (:import (java.io File FileInputStream)
           (org.apache.poi.xssf.usermodel XSSFWorkbook)))

(def ^:dynamic *ex-mngr-tmp-dir* nil)

;;FIXTURES
(defn excel-manager-fixture [f]
  (binding [*ex-mngr-tmp-dir* (th/mkdir-tmp)]
    (th/load-tmp-dir *ex-mngr-tmp-dir*)
    (f)
    (th/delete-recursively *ex-mngr-tmp-dir*)))

(use-fixtures :once excel-manager-fixture)

;; TESTS
(deftest generate-excel-test

  (testing "Throws exception if output dir is not supplied"
    (try
      (generate-excel nil nil)
      (catch Exception e
        (is (= (.getMessage e) "Invalid input dir"))
        (is (= nil (:file (ex-data e)))))))

  (testing "Throws exception if output dir is invalid"
    (let [some-rand-dir (File. (str "some-dir" (rand)))]
      (try
        (generate-excel nil some-rand-dir)
        (catch Exception e
          (is (= (.getMessage e) "Invalid input dir"))
          (is (= some-rand-dir (:file (ex-data e))))))))

  (testing "Generate a valid excel file"
    (let [ouput-file (File/createTempFile "zaf-valid-excel" ".xlsx")]
      (do
        (generate-excel (.getAbsolutePath ouput-file) *ex-mngr-tmp-dir*)
        (let [fis (FileInputStream. ouput-file)
              workbook (XSSFWorkbook. fis)
              message-map (etp/map-from-sheet (.getSheet workbook "message"))
              validation-map (etp/map-from-sheet (.getSheet workbook "validation"))
              ]
          (is (= 4 (count (keys message-map))))
          (is (= 6 (count (get message-map "message.properties"))))
          (is (= "Header" (get (get message-map "message.properties") "page1.header")))
          (is (= 4 (count (get message-map "message_en.properties"))))
          (is (= "Title" (get (get message-map "message_en.properties") "page3.title")))
          (is (= 4 (count (get message-map "message_es.properties"))))
          (is (= "Cabecera 2" (get (get message-map "message_es.properties") "page2.header")))
          (is (= 6 (count (get message-map "message_fr.properties"))))
          (is (= "La description" (get (get message-map "message_fr.properties") "page3.description")))

          (is (= 4 (count (keys validation-map))))
          (is (= 3 (count (get validation-map "validation.properties"))))
          (is (= "Error on page 1" (get (get validation-map "validation.properties") "page1.error")))
          (is (= 2 (count (get validation-map "validation_en.properties"))))
          (is (= "Error on page 2" (get (get validation-map "validation_en.properties") "page2.error")))
          (is (= 2 (count (get validation-map "validation_es.properties"))))
          (is (not (nil? (get (get validation-map "validation_es.properties") "page2.error"))))
          (is (= 2 (count (get validation-map "validation_fr.properties"))))
          (is (not (nil? (get (get validation-map "validation_fr.properties") "page1.error")))))
        (.delete ouput-file)))))

(deftest gen-ex-test
  (testing "routing properly to generate-excel"
    (with-redefs [generate-excel (fn [ouptut-excel-file input-dir] {:o (.getPath ouptut-excel-file) :i (.getPath input-dir)})]
      (is (= {:o "the_excel.xlsx", :i "PR_DIR"} (gen-ex "the_excel.xlsx" "PR_DIR"))))))
