(ns zaf.i18n-excel-tool.file-utils-test
  (:require [clojure.test :refer :all]
            [zaf.i18n-excel-tool.file-utils :refer :all]
            [zaf.i18n-excel-tool.test-helper :as th]
            [clojure.java.io :as io])
  (:import (java.io File)))

(def ^:dynamic *path-utl-tmp-dir* nil)

;; FIXTURES
(defn path-utils-fixture [f]
  (binding [*path-utl-tmp-dir* (th/mkdir-tmp)]
    (th/load-tmp-dir *path-utl-tmp-dir*)
    (f)
    (th/delete-recursively *path-utl-tmp-dir*)))

(use-fixtures :once path-utils-fixture)

;; TESTS
(deftest extract-lang-test

  (testing "Validating base file (no lang ext)"
    (is (= {:name "message" :lang ""} (extract-lang (File. "c:\\temp\\message.properties")))))

  (testing "Validating file with lang"
    (is (= {:name "validation" :lang "fr"} (extract-lang (File. "/temp/validation_fr.properties")))))

  (testing "Validating base file with lang and country)"
    (is (= {:name "message" :lang "en_CA"} (extract-lang (File. "c:\\temp\\message_en_CA.properties"))))))

(deftest props-file-map-test
  (testing "validating right structure of file-map"
    (let [file-map (props-file-map *path-utl-tmp-dir*)]
      (is (= 2 (count (keys file-map))))
      (is (= 4 (count (get file-map "message"))))
      (is (.startsWith (:path (first (get file-map "validation"))) (.getAbsolutePath *path-utl-tmp-dir*))))))

(deftest load-prop-test
  (testing "load properties object form properties file"
    (let [props (load-prop (str *path-utl-tmp-dir* (File/separator) "message_en.properties") "iso-8859-1")]
      (is (= 4 (count (.keySet props))))
      (is (= "Header" (.get props "page1.header")))
      (is (= "Working with internationalization" (.get props "page1.content")))
      (is (= "Title" (.get props "page3.title")))
      (is (= "Description" (.get props "page3.description"))))))

(deftest persist-prop-file-test
  (testing "save a properties file from a map"
    (let [path (th/touch-tmp)
          map {"table.title" "title" "header.text" "value2" "part.value" "part1"}]
      (do
        (persist-prop-file map path "iso-8859-1")
        (let [lines (clojure.string/split-lines (slurp path))]
          (is (= 4 (count lines)))
          (is (every? (set lines) ["table.title=title" "header.text=value2" "part.value=part1"]))
          (is (clojure.string/starts-with? (first lines) "#")))
        (io/delete-file path)))))

(deftest sort-file-content-test
  (testing "sort a property file content"
    (let [path (th/touch-tmp)
          lines ["#some header" "z1=value1" "a1=value2" "f3=value3"]]
      (do
        (spit path (clojure.string/join (System/lineSeparator) lines))
        (sort-file-content path "iso-8859-1")
        (let [[line1 line2 line3] (clojure.string/split-lines (slurp path))]
          (is (= "a1=value2" line1))
          (is (= "f3=value3" line2))
          (is (= "z1=value1" line3)))
        (io/delete-file path)))))

