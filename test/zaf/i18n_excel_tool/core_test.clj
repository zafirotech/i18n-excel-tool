(ns zaf.i18n-excel-tool.core-test
  (:require [clojure.test :refer :all]
            [zaf.i18n-excel-tool.core :refer :all]
            [zaf.i18n-excel-tool.convert-excel-to-props :refer [gen-pr]]
            [zaf.i18n-excel-tool.convert-props-to-excel :refer [gen-ex]]))

(deftest -main-test
  (testing "validating generation of excel from props"
    (with-redefs [gen-pr (fn [output_dir input_excel] {:o output_dir :i input_excel})]
      (is (= {:o "PR_DIR", :i "the_excel.xlsx"} (-main "-i" "the_excel.xlsx" "-o" "PR_DIR" "gen-pr")))))

  (testing "validating generation of props from excel"
    (with-redefs [gen-ex (fn [output_excel input_dir] {:o output_excel :i input_dir})]
      (is (= {:o "the_excel.xlsx", :i "PR_DIR"} (-main "-o" "the_excel.xlsx" "-i" "PR_DIR" "gen-ex"))))))
