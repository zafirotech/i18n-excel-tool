(ns zaf.i18n-excel-tool.convert-excel-to-props-test
  (:require [clojure.test :refer :all]
            [zaf.i18n-excel-tool.convert-excel-to-props :refer :all]
            [zaf.i18n-excel-tool.test-helper :as th]
            [clojure.java.io :as io])
  (:import (java.io File FileInputStream)
           (org.apache.poi.xssf.usermodel XSSFWorkbook)))

(deftest row-map-test
  (testing "validating structure of row-map"
    (let [key "panel.title"
          headers '("Default" "en" "fr" "es")
          values '("def panel title" "panel title" "titre du panneau" "titulo de panel")
          sheet-name "application"
          row-map (row-map key headers values sheet-name)]
      (is (= 4 (count row-map)))
      (is (= (get row-map "application.properties") {"panel.title" "def panel title"}))
      (is (= (get row-map "application_en.properties") {"panel.title" "panel title"}))
      (is (= (get row-map "application_fr.properties") {"panel.title" "titre du panneau"}))
      (is (= (get row-map "application_es.properties") {"panel.title" "titulo de panel"})))))

(deftest map-from-sheet-test
  (testing "valid structure on lang-map"
    (let [excel-file (io/file (io/resource "lang-doc.xlsx"))
          fis (FileInputStream. excel-file)
          workbook (XSSFWorkbook. fis)
          sheet (.getSheet workbook "message")
          lang-map (map-from-sheet sheet)]
      (is (= 4 (count lang-map)))
      (is (= 6 (count (get lang-map "message.properties"))))
      (is (= 4 (count (get lang-map "message_en.properties"))))
      (is (= 4 (count (get lang-map "message_es.properties"))))
      (is (= 6 (count (get lang-map "message_fr.properties"))))
      (is (= "Header" (get (get lang-map "message.properties") "page1.header")))
      (is (nil? (get (get lang-map "message_es.properties") "page3.title"))))))

(deftest generate-all-props-test
  (testing "testing  props file generator from excel method"
    (let [excel-file (io/file (io/resource "lang-doc.xlsx"))
          output-dir (th/mkdir-tmp)]
      (do
        (generate-all-props output-dir excel-file)
        (let [prop-files (drop 1 (file-seq output-dir))
              sample-content1 (slurp (str (.getAbsolutePath output-dir) (File/separator) "message_es.properties"))
              sample-content2 (slurp (str (.getAbsolutePath output-dir) (File/separator) "message_en.properties"))]
          (is (= 8 (count prop-files)))
          (is (clojure.string/includes? sample-content1 "page1.header=Cabecera"))
          (is (clojure.string/includes? sample-content2 "page3.description=Description")))
        (th/delete-recursively output-dir))))

  (testing "throws exception if invalid excel file is supplied"
    (let [some-rand-file (File. (str "some-file" (rand)))]
      (try
        (generate-all-props "." some-rand-file)
        (catch Exception e
          (is (= (.getMessage e) "Invalid input file"))
          (is (= some-rand-file (:file (ex-data e)))))))))

(deftest gen-pr-test
  (testing "routing propertly to generate-all-props-test"
    (with-redefs [generate-all-props (fn [output-dir input-excel-file] {:o (.getPath output-dir) :i (.getPath input-excel-file)})]
      (is (= {:o "PR_DIR", :i "the_excel.xlsx"} (gen-pr "PR_DIR" "the_excel.xlsx"))))))
