(ns zaf.i18n-excel-tool.test-helper
  (:require [clojure.java.io :as io])
  (:import (java.io File)))

(defn touch-tmp
  "Create tmp file"
  []
  (File/createTempFile "path-utils", nil))

(defn mkdir-tmp
  "Creates tmp dir"
  []
  (let [tmp-file (touch-tmp)]
    (do
      (.delete tmp-file)
      (.mkdir tmp-file)
      tmp-file)))

(defn load-tmp-dir
  "Load resource folder into tmp dir"
  [tmp-dir]
  (let [i18n-dir (io/file (io/resource "i18n"))
        files (file-seq i18n-dir)]
    (doseq [f files]
      (if (.isFile f)
        (io/copy f (io/file (str (.getAbsolutePath tmp-dir) (File/separator) (.getName f))))))))

(defn delete-recursively [fname]
  (doseq [f (reverse (file-seq (io/file fname)))]
    (io/delete-file f)))
