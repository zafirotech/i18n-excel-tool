(ns zaf.i18n-excel-tool.convert-props-to-excel
  (:require [clojure.java.io :as io]
            [zaf.i18n-excel-tool.file-utils :as fu]
            [zaf.i18n-excel-tool.configuration :refer [conf]])
  (:import (java.io FileOutputStream FileInputStream File)
           (org.apache.poi.xssf.usermodel XSSFSheet XSSFWorkbook)))

;;FROM PROPS to EXCEL
(defn append-content-sheet [sheet-name file-entries workbook]
  (let [sheet (.createSheet workbook sheet-name)
        sorted-entries (sort-by #(:lang %) file-entries)
        sorted-props (map #(fu/load-prop (:path %) "iso-8859-1") sorted-entries)
        sorted-headers (map #(:lang %) sorted-entries)
        all-keys-sorted (sort (set (reduce #(concat %1 (.keySet %2)) [] sorted-props)))]
    (do
      ;;header
      (let [header-row (.createRow sheet 0)
            first-header-cell (.createCell header-row 0)
            ;;TODO take out cosmetic changes to a post process function - leave content only here
            cell-width (* 256 (conf :default-cell-width))]
        (do
          (.setCellValue first-header-cell "Keys")
          (.setColumnWidth sheet 0, cell-width)
          (doseq [[header-cell-index header] (map-indexed vector sorted-headers)]
            (let [current-cell-index (inc header-cell-index)
                  current-header-cell (.createCell header-row current-cell-index)]
              (do
                (.setCellValue current-header-cell (if (clojure.string/blank? header) (conf :header-text-for-non-lang) header))
                (.setColumnWidth sheet current-cell-index cell-width))))))
      ;;body
      (doseq [key all-keys-sorted]
        (let [row (.createRow sheet (inc (.getLastRowNum sheet)))
              first-cell (.createCell row 0)]
          (do
            (.setCellValue first-cell key)
            (doseq [[cell-index prop] (map-indexed vector sorted-props)]
              (let [current-cell (.createCell row (inc cell-index))]
                (.setCellValue current-cell (.getProperty prop key ""))))))))))

(defn append-content-sheets [workbook input-dir]
  (let [props-file-map (fu/props-file-map input-dir)]
    (doseq [sheet-name (keys props-file-map)]
      (append-content-sheet sheet-name (get props-file-map sheet-name) workbook))))

(defn generate-excel
  "Generate excel document based on existing property files dir"
  [ouptut-excel-file input-dir]
  (if (and input-dir (.isDirectory input-dir))
    (let [excel-file-name (if ouptut-excel-file ouptut-excel-file (conf :default-excel-filename))
          workbook (XSSFWorkbook.)
          fos (FileOutputStream. excel-file-name)
          ]
      (do
        (append-content-sheets workbook input-dir)
        (.write workbook fos)
        (.close fos)))
    (throw (ex-info "Invalid input dir" {:file input-dir}))))

(defn gen-ex
  [ouptut-excel-file input-dir]
  "Wraps (generate-excel) replacing outputs/inputs if not supplied"
  (generate-excel
    (File. (if ouptut-excel-file ouptut-excel-file (conf :default-excel-output-path)))
    (File. (if input-dir input-dir (conf :default-input-directory)))))