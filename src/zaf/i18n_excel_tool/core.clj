(ns zaf.i18n-excel-tool.core
  (:require [clojure.tools.cli :refer [parse-opts]]
            [zaf.i18n-excel-tool.convert-excel-to-props :refer [gen-pr]]
            [zaf.i18n-excel-tool.convert-props-to-excel :refer [gen-ex]])
  (:gen-class))

(def cli-options
  [["-o" "--output OUTPUT" "Output Folder if creating Props or File if generating excel"]
   ["-i" "--input   INPUT" "Input Folder if creating excel or Folder if creating props"]
   ["-h" "--help"]])

(defn usage
  [options-summary]
  (->> ["Tool to convert from i18n properties file into excel and viceversa."
        ""
        "Usage: i18n-excel-tool [options] action"
        "Options:"
        options-summary
        ""
        "Actions:"
        "gen-ex  Generate excel file from i18n properties files"
        "gen-pr  Generate 18n properties files from excel file"]
       (clojure.string/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:"
       (System/lineSeparator) (System/lineSeparator)
       (clojure.string/join \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -main
  [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (not= (count arguments) 1) (exit 1 (usage summary))
      errors (exit 1 (error-msg errors))
      )
    (case (first arguments)
      "gen-ex" (gen-ex (:output options) (:input options))
      "gen-pr" (gen-pr (:output options) (:input options))
      (exit 1 (usage summary)))))


