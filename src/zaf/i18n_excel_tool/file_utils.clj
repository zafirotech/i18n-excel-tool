(ns zaf.i18n-excel-tool.file-utils
  (:require [clojure.java.io :as io])
  (:import (java.util Properties)))

(defn extract-lang
  "extract lang and name from file"
  [file]
  (let [filename (.getName file)
        match (re-matches #"^([^_]+)\_?(\w*)\.properties$" filename)]
    {:name (second match) :lang (nth match 2)}))

(defn props-file-map
  "generates a map for every i18n properties file in the directory. Map structure is like example:

  {message [{:path SOME_DIR\\message.properties, :lang }
            {:path SOME_DIR\\message_en.properties, :lang en}],
   validation [{:path SOME_DIR\\validation.properties, :lang }
               {:path SOME_DIR\\validation_en.properties, :lang en}]}
  "
  [dir]
  (let [file-dir (io/file dir)
        files (file-seq file-dir)]
    (reduce
      (fn [i18n-map file]
        (if (and (.isFile file) (.endsWith (.getName file) "properties"))
          (let [lang (extract-lang file)
                existing-list (get i18n-map (:name lang) [])
                current-list (conj existing-list {:path (.getAbsolutePath file) :lang (:lang lang)})]
            (assoc i18n-map (:name lang) current-list))
          i18n-map))
      {} files)))

(defn load-prop
  "Return a Java properties file based on a path"
  [prop-path encoding]
  (let [props (Properties.)]
    (do
      (with-open [r (io/reader prop-path :encoding encoding)]
        (.load props r))
      props)))

(defn persist-prop-file
  "Persist a map into a property file"
  [map prop-path encoding]
  (let [props (Properties.)]
    (do
      (doseq [[k v] map]
        (.put props k v))
      (with-open [w (io/writer prop-path :encoding encoding)]
        (.store props w nil)))))

(defn sort-file-content
  "Sort alphabetically content of the file also stripping first line to take rid of Properties first line generator"
  [path encoding]
  (let [raw-content (slurp path :encoding encoding)
        lines (drop 1 (clojure.string/split-lines raw-content))
        sorted-lines (sort lines)
        sorted-content (clojure.string/join (System/lineSeparator) sorted-lines)]
    (spit path sorted-content :encoding encoding)))
