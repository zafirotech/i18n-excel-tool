(ns zaf.i18n-excel-tool.convert-excel-to-props
  (:require [clojure.java.io :as io]
            [zaf.i18n-excel-tool.file-utils :as fu]
            [zaf.i18n-excel-tool.configuration :refer [conf]])
  (:import (java.io FileOutputStream FileInputStream File)
           (org.apache.poi.xssf.usermodel XSSFSheet XSSFWorkbook)))

;; FROM EXCEL to PROPS

(defn row-map
  "Extract a map from a sheet row"
  [key headers values sheet-name]
  (let [header-value-dict (map (fn [h v] {:h h :v v}) headers values)]
    (reduce (fn [r-map h-v-dict]
              (let [h (:h h-v-dict)
                    v (:v h-v-dict)
                    def-h (conf :header-text-for-non-lang)
                    filename (str sheet-name (if (= h def-h) "" (str "_" h)) ".properties")]
                (if (clojure.string/blank? v)
                  r-map
                  (assoc r-map filename {key v}))))
            {}
            header-value-dict)))

(defn map-from-sheet
  "Extract a map (key: name of the prop file, value: map with key/values of the prop content) from the Sheet"
  [sheet]
  (let [header-row (.getRow sheet 0)
        sheet-name (.getSheetName sheet)
        header-seq (map #(.getStringCellValue %) (drop 1 (iterator-seq (.cellIterator header-row))))
        row-seq (drop 1 (iterator-seq (.rowIterator sheet)))
        ]
    (reduce (fn [lang-map row]
              (let [cell-seq (iterator-seq (.cellIterator row))
                    [key & values] (map #(.getStringCellValue %) cell-seq)
                    row-map (row-map key header-seq values sheet-name)]
                (merge-with merge lang-map row-map)))
            {}
            row-seq)))

(defn generate-props-in-sheet
  "Generate all properties files for a sheet"
  [sheet output-dir]
  (let [lang-map (map-from-sheet sheet)]
    (doseq [key (keys lang-map)]
      (let [path (str output-dir (File/separator) key)]
        (do
          (fu/persist-prop-file (get lang-map key) path (conf :charset-encoding))
          (fu/sort-file-content path (conf :charset-encoding)))))))

(defn generate-all-props
  "Generate properties files based on and existing excel document"
  [output-dir input-excel-file]
  (if (and input-excel-file (.isFile input-excel-file))
    (let [dir (if output-dir output-dir (conf :default-output-directory))
          fis (FileInputStream. input-excel-file)
          workbook (XSSFWorkbook. fis)
          sheet-seq (iterator-seq (.sheetIterator workbook))]
      (doseq [sheet sheet-seq]
        (generate-props-in-sheet sheet dir)))
    (throw (ex-info "Invalid input file" {:file input-excel-file}))))

(defn gen-pr
  "Wraps (generate-all-props) replacing outputs/inputs if not supplied"
  [output-dir input-excel-file]
  (generate-all-props
    (File. (if output-dir output-dir (conf :default-output-directory)))
    (File. (if input-excel-file input-excel-file (conf :default-excel-input-path)))))