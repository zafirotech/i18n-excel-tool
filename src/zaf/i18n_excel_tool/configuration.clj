(ns zaf.i18n-excel-tool.configuration
  (:require [clojure.java.io :as io]
            [zaf.i18n-excel-tool.file-utils :as fu])
  (:import (java.io FileOutputStream FileInputStream File)
           (org.apache.poi.xssf.usermodel XSSFSheet XSSFWorkbook)))

(def config (read-string (slurp (io/resource "config.clj"))))

(defn conf
  "Return a configuration value based on keyword"
  [keyword]
  (get config keyword))
